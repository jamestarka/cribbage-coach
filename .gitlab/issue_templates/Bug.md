## Description
**Provide a brief description of the bug.**

TODO (required)

## Expected Behavior
**Describe what should happen.**

TODO (required)

## Actual Behavior
**Describe what did or does happen.**

TODO (required)

## Steps to Reproduce
**Provide the following information and steps to reproduce.**
- Application version: TODO
- Clojure version: TODO
- Operating System: TODO

TODO (optional, but recommended)

## Examples or Logs
**Provide any minimal code examples, links to sample projects, or links to logs that demonstrate or allow replication of the issue.**

TODO (optional, but recommended)
