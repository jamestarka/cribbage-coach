## Feature
**Provide a brief description of the feature.**

TODO

## Requirements
**Describe the scope of the feature. This should include what is the minimum for the feature to be considered useful, as well as optional "nice-to-haves."**

TODO

## Example
**Provide an example of how the feature might be used, for example code that uses a new function. This may be used to guide the API design.**

TODO

## Priority/Severity
**Remove the options that do not apply. The option chosen can be customized to provide specific details, if appropriate.**
- High: A signficant improvement to the application, bordering on essential
- Medium: A "nice-to-have" - not necessarily essential to the application's usage.
- Low: Anything else - minor, almost trivial improvements

## Other Comments
**Provide anything else that might be useful. This could include proposed solutions, concerns on performance impact, etc.**

TODO
