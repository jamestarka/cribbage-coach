# Cribbage-Coach

A Clojure-based command-line application for scoring and analyzing hands in the card game Cribbage.

## Status

This application is in its alpha stages. Most of the API is not yet determined and is subject to breaking changes. This code should not be considered production-ready; use at your own risk.

## Installation

No releases or executables exist. The best way to use the application is by building and running from the source.

### From Source
You will need Clojure installed on your system. You can install it from the [official installation instructions](https://clojure.org/guides/getting_started) or from one of the numerous guides available online. This project should work with all of the recent versions of Clojure, but development has been done using version 1.10.

You will also need [Leiningen](https://github.com/technomancy/leiningen) installed. Again, the project has installation instructions or the instructions can be found by searching your preferred search engine for Leiningen installation tutorials.

Once you have Clojure and Leiningen installed, you can download or clone this repository to your work environment.

    $ git clone git@gitlab.com:jamestarka/cribbage-coach.git

The executable jar can be built by running the following from the root project directory:

    $ lein uberjar

TODO add instructions for running with Lein

## Usage

Cribbage-Coach can be run from the jar

    $ java -jar cribbage-coach-0.1.0-standalone.jar [command] [options]

or directly from the source directory using Leiningen

    $ lein run [command] [options]

The valid commands and options are detailed below. In the following, a card is defined as a suit and a rank with codes given in the table below. Unless specified, any argument that takes a card (or uses these codes) is case-insensitive.

| Suit | Code | Rank | Code |
|------|------|------|------|
| Diamonds | D | Ace | A
| Hearts | H | 2 | 2
| Spades | S | 3 | 3
| Clubs | C | 4 | 4
| | | 5 | 5
| | | 6 | 6
| | | 7 | 7
| | | 8 | 8
| | | 9 | 9
| | | 10 | 10
| | | Jack | J
| | | Queen | Q
| | | King | K

### **score [card\*\*]**

Score the given Cribbage hand, given as a space-separated list of rank-suit paris.

*Examples:*

    $ lein run score "AC 5H 10C JH"
    4
    $ lein run score "2H 2C 2S 9D"
    8

## Tests

Tests can be run using Lein from the root of the project directory.

    $ lein test

## Contributing
Pull requests are accepted.

## License

[MIT](LICENSE)
