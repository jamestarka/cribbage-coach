;; SPDX-License-Identifier: MIT
(ns cribbage-coach.math
  (:require [clojure.set :as set]))

(defn sum
  "Returns the sum of the given values."
  [values]
  (apply + values))

(defn factorial
  "Returns the factorial of the given number."
  [n]
  (loop [number n accumulator 1]
    (if (zero? number)
      accumulator
      (recur (dec number) (* accumulator number)))))

;; TODO add better documentation for this method
(defn binomial-coefficient
  "Returns the binomial coefficient..."
  [n k]
  (/ (factorial n) (* (factorial k) (factorial (- n k)))))

(defn power-set
  "Returns the powerset of the given collection."
  [coll]
  (loop [c coll sets #{#{}}]
    (if (empty? c)
      sets
      (recur (into #{} (rest c))
             (set/union sets (into #{} (map #(conj % (first c)) sets)))))))

(defn not-any-proper-supersets?
  "Returns true if the given set does not have any supersets in the given sets."
  [s sets]
  (not-any? #(and (not (= s %))
                  (set/superset? % s)) sets))
