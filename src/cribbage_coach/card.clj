;; SPDX-License-Identifier: MIT
(ns cribbage-coach.card)

(def suits
  "The valid card suits in a deck of playing cards."
  [:spades :diamonds :hearts :clubs])

(def ranks
  "The valid card ranks in a deck of playing cards."
  [:ace :two :three :four :five :six :seven :eight :nine :ten :jack :queen :king])

(def suit-codes
  "The codes associated with each playing card suit."
  {:spades "S"
   :diamonds "D"
   :hearts "H"
   :clubs "C"})

(def rank-codes
  "The codes associated with each playing card rank."
  {:ace "A"
   :two "2"
   :three "3"
   :four "4"
   :five "5"
   :six "6"
   :seven "7"
   :eight "8"
   :nine "9"
   :ten "10"
   :jack "J"
   :queen "Q"
   :king "K"})

(defn make-card
  "Returns a card with the given suit and rank."
  [rank suit]
  {:rank rank :suit suit})

(defn make-spade
  "Returns a card with the given rank and the suit spades."
  [rank]
  (make-card rank :spades))

(defn make-heart
  "Returns a card with the given rank and the suit hearts."
  [rank]
  (make-card rank :hearts))

(defn make-diamond
  "Returns a card with the given rank and the suit diamonds."
  [rank]
  (make-card rank :diamonds))

(defn make-club
  "Returns a card with the given rank and the suit clubs."
  [rank]
  (make-card rank :clubs))

(defn same-suit?
  "Returns true if every card has the same suit, else false."
  [cards]
  (if (empty? cards)
    true
    (let [first-suit (:suit (first cards))]
      (every? #(= first-suit (:suit %)) cards))))

(def deck
  "A standard deck of 52 playing cards."
  (for [suit suits
        rank ranks]
    (make-card rank suit)))

(defn remaining-cards-in-deck
  "Returns the cards that remain in a deck after the given ones have been removed."
  [cards]
  (filter (fn [deck-card] (not-any? #(= % deck-card) cards))
          deck))

(defn random-n-cards
  "Returns n random cards from a full deck of cards."
  [n]
  (take n (shuffle deck)))
