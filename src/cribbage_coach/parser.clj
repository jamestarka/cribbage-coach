;; SPDX-License-Identifier: MIT
(ns cribbage-coach.parser
  (:require [clojure.string :as str]
            [cribbage-coach.card :as card]))

(defn parse-rank-code
  "Returns the card rank associated with the given code if it exists, otherwise nil."
  [code]
  (if (nil? code)
    nil
    (->> (filter #(= (second %) (str/upper-case code)) card/rank-codes)
         (first)
         (first))))

(defn parse-suit-code
  "Returns the card suit associated with the given code if it exists, otherwise nil"
  [code]
  (if (nil? code)
    nil
    (->> (filter #(= (second %) (str/upper-case code)) card/suit-codes)
         (first)
         (first))))

(defn- make-card-parse-error
  ;; TODO: documentation
  [message index]
  {:message message :index index})

;; TODO clean this method up
(defn- parse-card-state
  ;; TODO: add documentation
  [characters index rank suit]
  (if (empty? characters)
    ;; handle case where there is no more input
    (if rank
      (if suit
        {:card (card/make-card rank suit)}
        {:error (make-card-parse-error "Missing suit" index)})
      (if suit
        {:error (make-card-parse-error "Missing rank" index)}
        {:error (make-card-parse-error "Missing values" index)}))
    ;; handle case where additional input exists
    (if rank
      (if suit
        {:error (make-card-parse-error "Unexpected value(s)" index)}
        (if-let [parsed-suit (parse-suit-code (first characters))]
          (parse-card-state (rest characters) (+ 1 index) rank parsed-suit)
          {:error (make-card-parse-error "Unexpected suit" index)}))
      (if suit
        (if-let [parsed-rank-2 (parse-rank-code (apply str (take 2 characters)))]
          (parse-card-state (rest (rest characters)) (+ (count (card/rank-codes parsed-rank-2)) index) parsed-rank-2 suit)
          (if-let [parsed-rank-1 (parse-rank-code (first characters))]
            (parse-card-state (rest characters) (+ 1 index) parsed-rank-1 suit)
            {:error (make-card-parse-error "Unexpected rank" index)}))
        (if-let [parsed-suit (parse-suit-code (first characters))]
          (parse-card-state (rest characters) (+ 1 index) nil parsed-suit)
          (if-let [parsed-rank-2 (parse-rank-code (apply str (take 2 characters)))]
            (parse-card-state (rest (rest characters)) (+ (count (card/rank-codes parsed-rank-2)) index) parsed-rank-2 nil)
            (if-let [parsed-rank-1 (parse-rank-code (first characters))]
              (parse-card-state (rest characters) (+ 1 index) parsed-rank-1 nil)
              {:error (make-card-parse-error "Unexpected rank" index)})))))))

;; TODO rewrite using loop...
(defn parse-card
  "Returns a map that contains the parsed card (:card) if the card can be parsed successfully or the error message associated with the parse failure (:error).
   
   Supports input as rank-first and suit-first. Examples:
   AS (ace of spades)
   as (ace of spades)
   SA (ace of spades)
   5D (five of diamonds)
   10H (ten of hearts)
   JC (jack of clubs)"
  [v]
  (parse-card-state (vec v) 0 nil nil))

;; TODO fix the documentation for this method
(defn parse-cards
  "Returns "
  [v]
  (let [initial {:cards [] :errors []}]
    (if (str/blank? v)
      initial
      (let [splits (str/split v #" +")]
        (->> (map #(parse-card %) splits)
             (reduce (fn [result v]
                       (if (contains? v :error)
                         (update result :errors #(conj % (:error v)))
                         (update result :cards #(conj % (:card v)))))
                     initial))))))
