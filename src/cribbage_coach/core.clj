;; SPDX-License-Identifier: MIT
(ns cribbage-coach.core
  (:require [cribbage-coach.card :as card]
            [cribbage-coach.math :as math]
            [cribbage-coach.parser :as parser])
  (:gen-class))

(def rank-values
  "The cribbage values associated with each playing card rank."
  {:ace 1
   :two 2
   :three 3
   :four 4
   :five 5
   :six 6
   :seven 7
   :eight 8
   :nine 9
   :ten 10
   :jack 10
   :queen 10
   :king 10})

(def rank-run-order
  "The order of the racks for the purpose of computing runs."
  [:ace :two :three :four :five :six :seven :eight :nine :ten :jack :queen :king])

(defn card-value
  "Returns the value of the given card."
  [card]
  ((:rank card) rank-values))

(defn total-value
  "Returns the sum of the values of the given cards."
  [cards]
  (math/sum (map card-value cards)))

(defn count-pairs
  "Returns the number of pairs in the given cards."
  [cards]
  (->> (map :rank cards)
       (frequencies)
       (reduce-kv (fn [total _ v]
                    (if (<= v 1)
                      total ;; no new pairs
                      (+ total (math/binomial-coefficient v 2))))
                  0)))

(defn score-pairs
  "Returns the score from counting pairs in the given cards and the given starter card."
  [cards starter-card]
  (* 2 (count-pairs (conj cards starter-card))))

(defn fifteen?
  "Returns true if the total value of the cards is fifteen, otherwise false."
  [cards]
  (= 15 (total-value cards)))

(defn count-fifteens
  "Returns the number of fifteens that can be made from the given cards."
  [cards]
  (count (filter fifteen? (math/power-set cards))))

(defn score-fifteens
  "Returns the score from counting fifteens in the given cards and the given starter card.
   
   In cribbage, two points are awarded for each combination of cards whose values add to fifteen."
  [cards starter-card]
  (* 2 (count-fifteens (conj cards starter-card))))

(defn run-adjacent?
  "Returns true if the two given cards are adjacent to each other when considering a run."
  [card0 card1]
  (let [index0 (.indexOf rank-run-order (:rank card0))
        index1 (.indexOf rank-run-order (:rank card1))]
    (= 1 (java.lang.Math/abs (- index0 index1)))))

(defn run?
  "Returns true if the given cards represent a single run."
  [cards]
  (loop [sorted-cards (sort-by #(.indexOf rank-run-order (:rank %)) cards)]
    (cond
      (empty? sorted-cards) false
      (= 1 (count sorted-cards)) true
      :else (and (run-adjacent? (first sorted-cards) (second sorted-cards))
                 (recur (rest sorted-cards))))))

(defn score-runs
  "Returns the score from counting runs in the given cards and the given starter card."
  [cards starter-card]
  (let [runs (->> (math/power-set (conj cards starter-card))
                  ;; run must be of at least 3 cards, so filter out any that don't have three cards
                  (filter #(< 2 (count %)))
                  ;; keep only those that are runs
                  (filter #(run? %)))]
    ;; remove any "nested" runs - consider only the longest runs
    (->> (filter #(math/not-any-proper-supersets? % runs) runs)
         (map count)
         (reduce +))))

(defn score-hand-same-suit
  "Returns the score from counting the same suit in the given cards in a hand and the given starter card.
   
   In cribbage, if all cards have the same suit, one point is awarded per card. In a hand, the suits must match each other."
  [cards starter-card]
  (if (card/same-suit? (conj cards starter-card)) (+ (count cards) 1)
      (if (card/same-suit? cards) (count cards) 0)))

(defn score-crib-same-suit
  "Returns the score from counting the same suit in the given cards in the crib and the given starter card.
   
   In cribbage, if all cards have the same suit, one point is awarded per card. In the crib, they must also match the starter card."
  [cards starter-card]
  (if (card/same-suit? (conj cards starter-card)) (+ (count cards) 1) 0))

(defn nobs?
  "Returns true if the given cards have \"nobs\" with respect to the given starter card."
  [cards starter-card]
  (let [starter-suit (:suit starter-card)]
    (some #(and (= starter-suit (:suit %))
                (= :jack (:rank %))) cards)))

(defn score-nobs
  "Returns the score from considering \"nobs\" for the given cards and the given starter card."
  [cards starter-card]
  (if (nobs? cards starter-card) 1 0))

(defn score-hand
  "Returns the total cribbage score for the given cards in a hand and the given starter card."
  [cards starter-card]
  (+ (score-pairs cards starter-card)
     (score-fifteens cards starter-card)
     (score-runs cards starter-card)
     (score-hand-same-suit cards starter-card)
     (score-nobs cards starter-card)))

(defn score-crib
  "Returns the total cribbage score for the given cards in the crib and the given starter card."
  [cards starter-card]
  (+ (score-pairs cards starter-card)
     (score-fifteens cards starter-card)
     (score-runs cards starter-card)
     (score-crib-same-suit cards starter-card)
     (score-nobs cards starter-card)))

(defn hand-score-probabilities
  "Returns the probability of each score for the given cards assuming that the starter is unknown."
  [cards]
  (let [remaining-cards (card/remaining-cards-in-deck cards)
        remaining-cards-count (count remaining-cards)
        scores (map #(score-hand cards %) remaining-cards)
        score-frequencies (frequencies scores)]
    (reduce (fn [m [k v]]
              (assoc m k (#(/ % remaining-cards-count) v)))
            {} score-frequencies)))

(defn hand-expected-score
  "Returns the expected score value for the given cards assuming that the starter card is unknown."
  [cards]
  ;; assuming that each card in the deck not in "cards" is equally likely:
  ;; ExpectedValue(remaining cards) = Sum(Probability(remaining card) * Score(cards + remaining card)) 
  ;; Since all remaining cards are equally likely, this just becomes:
  ;; ExpectedValue (remaining cards) = Probability (remaining card) * Sum(Score(cards + each remaining card))
  ;; where Probability(remaining card) = 1 / (count remaining-cards)
  (let [remaining-cards (card/remaining-cards-in-deck cards)
        hand-scores (for [remaining-card remaining-cards]
                      (score-hand cards remaining-card))]
    (/ (math/sum hand-scores) (count remaining-cards))))

;; TODO move to a separate file....CLI?
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (if (empty? args)
    (println "At least one argument is required!")
    (let [parse-result (parser/parse-cards (first args))]
      (if (empty? (:errors parse-result))
        (let [parsed-cards (:cards parse-result)]
          (println (score-hand (butlast parsed-cards) (last parsed-cards))))
        ;; TODO actually display the errors here
        "found some errors!"))))
