;; SPDX-License-Identifier: MIT
(defproject cribbage-coach "0.1.0-SNAPSHOT"
  :description "A Clojure-based command-line application for scoring and analyzing hands in the card game Cribbage."
  :url "https://gitlab.com/jamestarka/cribbage-coach"
  :license {:name "MIT"
            :url "https://mit-license.org/"}
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :main ^:skip-aot cribbage-coach.core
  :target-path "target/%s"
  :profiles {:dev {:dependencies [[clj-kondo "2021.10.19"]]
                   :plugins [[lein-cljfmt "0.8.0"]
                             [lein-githooks "0.1.0"]]
                   :githooks {:pre-commit ["lein lint"]
                              :pre-push ["lein test"]}
                     :aliases {"clj-kondo-deps" ["clj-kondo" "--copy-configs" "--dependencies" "--lint" "$classpath"]
                               "clj-kondo-src-test" ["clj-kondo" "--lint" "src" "test"]
                               "clj-kondo-full" ["do" ["clj-kondo-deps"] ["clj-kondo-src-test"]]
                               "clj-kondo" ["run" "-m" "clj-kondo.main"]
                               "setup-local" ["do" ["deps"] ["clj-kondo-deps"] ["githooks" "install"]]
                               "lint" ["do" ["cljfmt" "check"] ["clj-kondo-src-test"]]}}
             :uberjar {:aot :all}}
)