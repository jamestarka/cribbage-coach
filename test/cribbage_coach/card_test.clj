;; SPDX-License-Identifier: MIT
(ns cribbage-coach.card-test
  (:require [clojure.test :refer [deftest is testing]]
            [cribbage-coach.card :as card]))

;; cards useful for testing purposes
(def ^:private ace-of-spades {:rank :ace :suit :spades})
(def ^:private five-of-diamonds {:rank :five :suit :diamonds})
(def ^:private two-of-hearts {:rank :two :suit :hearts})
(def ^:private jack-of-clubs {:rank :jack :suit :clubs})

(deftest make-card-valid-test
  (testing "valid cards made correctly"
    (is (= ace-of-spades (card/make-card :ace :spades)))
    (is (= five-of-diamonds (card/make-card :five :diamonds)))
    (is (= two-of-hearts (card/make-card :two :hearts)))
    (is (= jack-of-clubs (card/make-card :jack :clubs)))))

(deftest same-suit-test
  (testing "all cards have the same suit"
    (is (card/same-suit? []))
    (is (card/same-suit? [(card/make-spade :ace)]))
    (is (card/same-suit? [(card/make-spade :ace)
                          (card/make-spade :king)]))))

(deftest different-suit-test
  (testing "cards have different suits"
    (is (not (card/same-suit? [(card/make-spade :ace)
                               (card/make-heart :two)])))))

(deftest remaining-cards-test
  (testing "cards removed from a deck"
    (is (empty? (card/remaining-cards-in-deck card/deck)))
    (is (= card/deck (card/remaining-cards-in-deck [])))
    (let [minus-one-card (card/remaining-cards-in-deck [ace-of-spades])
          minus-two-cards (card/remaining-cards-in-deck [ace-of-spades five-of-diamonds])]
      (is (= 51 (count minus-one-card)))
      (is (not-any? #(= ace-of-spades %) minus-one-card))
      (is (= 50 (count minus-two-cards)))
      (is (not-any? #(= ace-of-spades %) minus-two-cards))
      (is (not-any? #(= five-of-diamonds %) minus-two-cards)))))

(deftest random-n-cards-test
  (testing "getting n random from a deck"
    (is (empty? (card/random-n-cards 0)))
    (is (= 1 (count (card/random-n-cards 1))))
    (let [size-two (card/random-n-cards 2)
          size-three (card/random-n-cards 3)
          size-four (card/random-n-cards 4)]
      (is (= 2 (count size-two)))
      (is (distinct? size-two))
      (is (= 3 (count size-three)))
      (is (distinct? size-three))
      (is (= 4 (count size-four)))
      (is (distinct? size-four)))))
