;; SPDX-License-Identifier: MIT
(ns cribbage-coach.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [cribbage-coach.card :as card]
            [cribbage-coach.core :as core]))

;; TODO is there a way to make a macro that generates all of these?
(def ^:private ace-of-spades (card/make-spade :ace))
(def ^:private four-of-spades (card/make-spade :four))
(def ^:private five-of-spades (card/make-spade :five))
(def ^:private six-of-spades (card/make-spade :six))
(def ^:private ten-of-spades (card/make-spade :ten))
(def ^:private jack-of-spades (card/make-spade :jack))
(def ^:private king-of-spades (card/make-spade :king))
(def ^:private ace-of-hearts (card/make-heart :ace))
(def ^:private five-of-hearts (card/make-heart :five))
(def ^:private seven-of-hearts (card/make-heart :seven))
(def ^:private nine-of-hearts (card/make-heart :nine))
(def ^:private jack-of-hearts (card/make-heart :jack))
(def ^:private ace-of-diamonds (card/make-diamond :ace))
(def ^:private two-of-diamonds (card/make-diamond :two))
(def ^:private four-of-diamonds (card/make-diamond :four))
(def ^:private five-of-diamonds (card/make-diamond :five))
(def ^:private nine-of-diamonds (card/make-diamond :nine))
(def ^:private jack-of-diamonds (card/make-diamond :jack))
(def ^:private three-of-clubs (card/make-club :three))
(def ^:private five-of-clubs (card/make-club :five))
(def ^:private eight-of-clubs (card/make-club :eight))
(def ^:private nine-of-clubs (card/make-club :nine))
(def ^:private jack-of-clubs (card/make-club :jack))
(def ^:private queen-of-clubs (card/make-club :queen))

(deftest card-value-test
  (testing "the value of a card is obtained correctly"
    (is (= 1 (core/card-value (card/make-spade :ace))))
    (is (= 2 (core/card-value (card/make-spade :two))))
    (is (= 3 (core/card-value (card/make-spade :three))))
    (is (= 4 (core/card-value {:rank :four})))
    (is (= 5 (core/card-value {:rank :five})))
    (is (= 6 (core/card-value {:rank :six})))
    (is (= 7 (core/card-value {:rank :seven})))
    (is (= 8 (core/card-value {:rank :eight})))
    (is (= 9 (core/card-value {:rank :nine})))
    (is (= 10 (core/card-value {:rank :ten})))
    (is (= 10 (core/card-value {:rank :jack})))
    (is (= 10 (core/card-value {:rank :queen})))
    (is (= 10 (core/card-value {:rank :king})))))

(deftest total-value-test
  (testing "the total value of some cards is obtained correctly"
    (is (= 0 (core/total-value [])))
    (is (= 1 (core/total-value [ace-of-spades])))
    (is (= 6 (core/total-value [ace-of-spades five-of-spades])))
    (is (= 11 (core/total-value [ace-of-spades king-of-spades])))
    (is (= 20 (core/total-value [king-of-spades jack-of-clubs])))))

(deftest count-pairs-test
  (testing "the number of pairs found in the given cards"
    (is (= 0 (core/count-pairs [])))
    (is (= 0 (core/count-pairs [ace-of-spades])))
    (is (= 0 (core/count-pairs [ace-of-spades five-of-spades])))
    (is (= 1 (core/count-pairs [ace-of-spades ace-of-diamonds])))
    (is (= 3 (core/count-pairs [ace-of-spades ace-of-diamonds ace-of-hearts])))))

;; TODO use property based testing to test that score is always twice the number of pairs
;; instead of copying the conditions directly from the above
(deftest score-pairs-test
  (testing "the number of pairs found in the given cards is scored correctly"
    (is (= 0 (core/score-pairs [] ace-of-spades)))
    (is (= 0 (core/score-pairs [ace-of-spades] two-of-diamonds)))
    (is (= 0 (core/score-pairs [ace-of-spades five-of-spades] two-of-diamonds)))
    (is (= 2 (core/score-pairs [ace-of-spades] ace-of-hearts)))
    (is (= 6 (core/score-pairs [ace-of-spades ace-of-diamonds] ace-of-hearts)))))

(deftest fifteens-test
  (testing "cards whose values total to fifteen"
    (is (core/fifteen? [five-of-hearts king-of-spades]))
    (is (core/fifteen? [ace-of-spades ace-of-diamonds three-of-clubs king-of-spades]))))

(deftest non-fifteens-test
  (testing "cards whose values do not total to fifteen"
    (is (not (core/fifteen? [])))
    (is (not (core/fifteen? [ace-of-spades ace-of-diamonds king-of-spades ace-of-hearts])))))

(deftest count-fifteens-test
  (testing "the number of combinations of cards whose values total fifteen"
    (is (= 0 (core/count-fifteens [])))
    (is (= 1 (core/count-fifteens [ace-of-hearts ace-of-spades five-of-hearts king-of-spades])))
    (is (= 2 (core/count-fifteens [ace-of-hearts jack-of-clubs five-of-hearts king-of-spades])))))

(deftest score-fifteens-test
  (testing "the score resulting from combinations of cards whose values total fifteen"
    (is (= 0 (core/score-fifteens [] ace-of-spades)))
    (is (= 2 (core/score-fifteens [ace-of-hearts ace-of-spades five-of-hearts king-of-spades] two-of-diamonds)))
    (is (= 4 (core/score-fifteens [ace-of-hearts jack-of-clubs five-of-hearts king-of-spades] two-of-diamonds)))))

(deftest run-adjacent-test
  (testing "the given cards are adjacent to each other in a run"
    (is (core/run-adjacent? ace-of-diamonds two-of-diamonds))
    (is (core/run-adjacent? two-of-diamonds ace-of-diamonds))
    (is (core/run-adjacent? two-of-diamonds three-of-clubs))
    (is (core/run-adjacent? three-of-clubs four-of-spades))
    (is (core/run-adjacent? four-of-spades five-of-hearts))
    (is (core/run-adjacent? five-of-hearts six-of-spades))
    (is (core/run-adjacent? six-of-spades seven-of-hearts))
    (is (core/run-adjacent? seven-of-hearts eight-of-clubs))
    (is (core/run-adjacent? eight-of-clubs nine-of-diamonds))
    (is (core/run-adjacent? nine-of-diamonds ten-of-spades))
    (is (core/run-adjacent? ten-of-spades jack-of-clubs))
    (is (core/run-adjacent? jack-of-clubs queen-of-clubs))
    (is (core/run-adjacent? queen-of-clubs king-of-spades))))

(deftest run-non-adjacent-test
  (testing "the given cards are not adjacent to each other in a run"
    (is (not (core/run-adjacent? ace-of-diamonds three-of-clubs)))
    (is (not (core/run-adjacent? ace-of-diamonds king-of-spades)))
    (is (not (core/run-adjacent? king-of-spades ace-of-diamonds)))
    (is (not (core/run-adjacent? jack-of-clubs king-of-spades)))
    (is (not (core/run-adjacent? ten-of-spades queen-of-clubs)))))

(deftest run-test
  (testing "the given cards are runs"
    (is (core/run? [ace-of-diamonds]))
    (is (core/run? [ace-of-diamonds two-of-diamonds three-of-clubs]))
    (is (core/run? [ace-of-diamonds three-of-clubs two-of-diamonds]))
    (is (core/run? [three-of-clubs ace-of-spades two-of-diamonds]))
    (is (core/run? [ten-of-spades jack-of-clubs queen-of-clubs]))
    (is (core/run? [jack-of-diamonds queen-of-clubs king-of-spades]))
    (is (core/run? [five-of-diamonds four-of-diamonds three-of-clubs two-of-diamonds ace-of-spades]))))

(deftest non-run-test
  (testing "the given cards are not runs"
    (is (not (core/run? [])))
    (is (not (core/run? [ace-of-diamonds ace-of-hearts])))
    (is (not (core/run? [ace-of-spades three-of-clubs four-of-diamonds])))
    (is (not (core/run? [ace-of-diamonds ace-of-hearts two-of-diamonds three-of-clubs])))
    (is (not (core/run? [queen-of-clubs king-of-spades ace-of-diamonds])))))

(deftest score-runs-test
  (testing "the scores of various hands"
    (is (= 0 (core/score-runs [ace-of-diamonds three-of-clubs king-of-spades jack-of-clubs] nine-of-diamonds)))
    (is (= 0 (core/score-runs [four-of-diamonds jack-of-spades five-of-spades nine-of-hearts] nine-of-diamonds)))
    (is (= 0 (core/score-runs [five-of-hearts five-of-diamonds five-of-spades king-of-spades] three-of-clubs)))
    (is (= 3 (core/score-runs [ace-of-diamonds two-of-diamonds three-of-clubs nine-of-diamonds] nine-of-clubs)))
    (is (= 8 (core/score-runs [four-of-spades five-of-spades six-of-spades five-of-hearts] three-of-clubs)))
    (is (= 0 (core/score-runs [ace-of-diamonds two-of-diamonds four-of-diamonds five-of-diamonds] nine-of-diamonds)))
    (is (= 5 (core/score-runs [ace-of-diamonds two-of-diamonds four-of-diamonds five-of-diamonds] three-of-clubs)))
    (is (= 0 (core/score-runs [five-of-hearts five-of-spades five-of-diamonds jack-of-clubs] five-of-clubs)))))

(deftest score-hand-same-suit-test
  (testing "the score when all cards have the same suit or different different in a hand"
    (is (= 0 (core/score-hand-same-suit [ace-of-spades five-of-spades king-of-spades ace-of-hearts] jack-of-spades)))
    (is (= 4 (core/score-hand-same-suit [ace-of-spades five-of-spades six-of-spades jack-of-spades] ace-of-hearts)))
    (is (= 5 (core/score-hand-same-suit [ace-of-spades five-of-spades six-of-spades jack-of-spades] king-of-spades)))))

(deftest score-crib-same-suit-test
  (testing "the score when all cards have the same suit or different suits in the crib"
    (is (= 0 (core/score-crib-same-suit [ace-of-spades five-of-spades king-of-spades ace-of-hearts] jack-of-spades)))
    (is (= 0 (core/score-crib-same-suit [ace-of-spades five-of-spades six-of-spades jack-of-spades] ace-of-hearts)))
    (is (= 5 (core/score-crib-same-suit [ace-of-spades five-of-spades six-of-spades jack-of-spades] king-of-spades)))))

(deftest nobs-test
  (testing "hands that have or don't have nobs"
    (is (not (core/nobs? [] ace-of-spades)))
    (is (not (core/nobs? [jack-of-clubs jack-of-hearts jack-of-diamonds five-of-hearts] ace-of-spades)))
    (is (core/nobs? [jack-of-clubs jack-of-hearts jack-of-diamonds five-of-hearts] three-of-clubs))
    (is (core/nobs? [three-of-clubs ace-of-diamonds jack-of-hearts nine-of-diamonds] five-of-hearts))))

(deftest score-nobs-test
  (testing "the score when a hand has or does not have nobs"
    (is (= 0 (core/score-nobs [] ace-of-spades)))
    (is (= 0 (core/score-nobs [jack-of-clubs jack-of-hearts jack-of-diamonds five-of-hearts] ace-of-spades)))
    (is (= 1 (core/score-nobs [jack-of-clubs jack-of-hearts jack-of-diamonds five-of-hearts] three-of-clubs)))
    (is (= 1 (core/score-nobs [three-of-clubs ace-of-diamonds jack-of-hearts nine-of-diamonds] five-of-hearts)))))

(deftest score-hand-test
  (testing "the scores of various hands"
    (is (= 0 (core/score-hand [ace-of-diamonds three-of-clubs king-of-spades jack-of-clubs] nine-of-diamonds)))
    (is (= 4 (core/score-hand [four-of-diamonds jack-of-spades five-of-spades nine-of-hearts] nine-of-diamonds)))
    (is (= 14 (core/score-hand [five-of-hearts five-of-diamonds five-of-spades king-of-spades] three-of-clubs)))
    (is (= 9 (core/score-hand [ace-of-diamonds two-of-diamonds three-of-clubs nine-of-diamonds] nine-of-clubs)))
    (is (= 14 (core/score-hand [four-of-spades five-of-spades six-of-spades five-of-hearts] three-of-clubs)))
    (is (= 9 (core/score-hand [ace-of-diamonds two-of-diamonds four-of-diamonds five-of-diamonds] nine-of-diamonds)))
    (is (= 11 (core/score-hand [ace-of-diamonds two-of-diamonds four-of-diamonds five-of-diamonds] three-of-clubs)))
    (is (= 29 (core/score-hand [five-of-hearts five-of-spades five-of-diamonds jack-of-clubs] five-of-clubs)))))

(deftest score-crib-test
  (testing "the scores of various cribs"
    (is (= 0 (core/score-crib [ace-of-diamonds three-of-clubs king-of-spades jack-of-clubs] nine-of-diamonds)))
    (is (= 4 (core/score-crib [four-of-diamonds jack-of-spades five-of-spades nine-of-hearts] nine-of-diamonds)))
    (is (= 14 (core/score-crib [five-of-hearts five-of-diamonds five-of-spades king-of-spades] three-of-clubs)))
    (is (= 9 (core/score-crib [ace-of-diamonds two-of-diamonds three-of-clubs nine-of-diamonds] nine-of-clubs)))
    (is (= 14 (core/score-crib [four-of-spades five-of-spades six-of-spades five-of-hearts] three-of-clubs)))
    (is (= 9 (core/score-crib [ace-of-diamonds two-of-diamonds four-of-diamonds five-of-diamonds] nine-of-diamonds)))
    (is (= 7 (core/score-crib [ace-of-diamonds two-of-diamonds four-of-diamonds five-of-diamonds] three-of-clubs)))
    (is (= 29 (core/score-crib [five-of-spades five-of-hearts five-of-diamonds jack-of-clubs] five-of-clubs)))))

(deftest hand-score-probabilities-test
  (testing "the probabilities of scores for the given hands"
    (is (= {0 5/16 1 5/48 2 1/6 3 1/12 4 7/48 5 1/24 6 1/24 7 1/12 8 1/48} (core/hand-score-probabilities [ace-of-diamonds three-of-clubs king-of-spades jack-of-clubs])))))

(deftest hand-expected-score-test
  (testing "the expected score for the given hands assuming the starter card is unknown"
    (is (= 119/48 (core/hand-expected-score [ace-of-diamonds three-of-clubs king-of-spades jack-of-clubs])))))
