;; SPDX-License-Identifier: MIT
(ns cribbage-coach.math-test
  (:require [clojure.test :refer [deftest is testing]]
            [cribbage-coach.math :as math]))

(deftest sum-test
  (testing "sum of values"
    (is (= 0 (math/sum [])))
    (is (= 5 (math/sum [5])))
    (is (= 10 (math/sum [1 2 3 4])))))

(deftest factorial-test
  (testing "factorial computed correctly"
    (is (= 1 (math/factorial 0)))
    (is (= 1 (math/factorial 1)))
    (is (= 2 (math/factorial 2)))
    (is (= 120 (math/factorial 5)))))

(deftest binomial-coefficient-test
  (testing "binomial coefficient computed correctly"
    (is (= 1 (math/binomial-coefficient 1 0)))
    (is (= 1 (math/binomial-coefficient 5 0)))
    (is (= 1 (math/binomial-coefficient 1 1)))
    (is (= 1 (math/binomial-coefficient 5 5)))
    (is (= 3 (math/binomial-coefficient 3 2)))
    (is (= 10 (math/binomial-coefficient 5 2)))))

(deftest power-set-test
  (testing "powersets of the given collections"
    (is (= #{#{}} (math/power-set #{})))
    (is (= #{#{} #{1}} (math/power-set #{1})))
    (is (= #{#{} #{1 2} #{1 3} #{2 3} #{1 2 3} #{1} #{2} #{3}} (math/power-set #{1 2 3})))))

(deftest not-any-proper-supersets-test
  (testing "the given set does not have proper supersets in the given collection of sets"
    (is (math/not-any-proper-supersets? #{} [#{}]))
    (is (math/not-any-proper-supersets? #{1} [#{}]))
    (is (math/not-any-proper-supersets? #{1} [#{1}]))
    (is (math/not-any-proper-supersets? #{1} []))
    (is (math/not-any-proper-supersets? #{1} [#{2} #{3}]))
    (is (math/not-any-proper-supersets? #{1} [#{3} #{2} #{1}]))))

(deftest any-proper-supersets-test
  (testing "the given set does have at keast one proper superset in the given collection of sets"
    (is (not (math/not-any-proper-supersets? #{} [#{1}])))
    (is (not (math/not-any-proper-supersets? #{1} [#{1 2}])))
    (is (not (math/not-any-proper-supersets? #{1} [#{1} #{1 2}])))
    (is (not (math/not-any-proper-supersets? #{1} [#{1 2} #{2}])))))
