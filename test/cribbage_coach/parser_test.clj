;; SPDX-License-Identifier: MIT
(ns cribbage-coach.parser-test
  (:require [clojure.test :refer [deftest is testing]]
            [cribbage-coach.card :as card]
            [cribbage-coach.parser :as parser]))

(def ^:private ace-of-spades (card/make-spade :ace))
(def ^:private five-of-diamonds (card/make-diamond :five))
(def ^:private ten-of-hearts (card/make-heart :ten))
(def ^:private jack-of-clubs (card/make-club :jack))

(deftest parse-rank-code-valid-test
  (testing "rank codes parsed correctly"
    (is (= :ace (parser/parse-rank-code "a")))
    (is (= :ace (parser/parse-rank-code "A")))
    (is (= :two (parser/parse-rank-code "2")))
    (is (= :three (parser/parse-rank-code "3")))
    (is (= :four (parser/parse-rank-code "4")))
    (is (= :five (parser/parse-rank-code "5")))
    (is (= :six (parser/parse-rank-code "6")))
    (is (= :seven (parser/parse-rank-code "7")))
    (is (= :eight (parser/parse-rank-code "8")))
    (is (= :nine (parser/parse-rank-code "9")))
    (is (= :ten (parser/parse-rank-code "10")))
    (is (= :jack (parser/parse-rank-code "j")))
    (is (= :jack (parser/parse-rank-code "J")))
    (is (= :queen (parser/parse-rank-code "q")))
    (is (= :queen (parser/parse-rank-code "Q")))
    (is (= :king (parser/parse-rank-code "k")))
    (is (= :king (parser/parse-rank-code "K")))))

(deftest parse-rank-code-invalid-test
  (testing "invalid rank codes parse to nil"
    (is (nil? (parser/parse-rank-code "")))
    (is (nil? (parser/parse-rank-code "z")))
    (is (nil? (parser/parse-rank-code nil)))))

(deftest parse-suit-code-valid-test
  (testing "suit codes parsed correctly"
    (is (= :spades (parser/parse-suit-code "s")))
    (is (= :spades (parser/parse-suit-code "S")))
    (is (= :diamonds (parser/parse-suit-code "d")))
    (is (= :diamonds (parser/parse-suit-code "D")))
    (is (= :hearts (parser/parse-suit-code "h")))
    (is (= :hearts (parser/parse-suit-code "H")))
    (is (= :clubs (parser/parse-suit-code "c")))
    (is (= :clubs (parser/parse-suit-code "C")))))

(deftest parse-suit-code-invalid-test
  (testing "invalid suit codes parse to nil"
    (is (nil? (parser/parse-suit-code "")))
    (is (nil? (parser/parse-suit-code "z")))
    (is (nil? (parser/parse-suit-code nil)))))

(deftest parse-valid-card-test
  (testing "valid cards parsed correctly"
    (is (= ace-of-spades (:card (parser/parse-card "as"))))
    (is (= ace-of-spades (:card (parser/parse-card "sa"))))
    (is (= five-of-diamonds (:card (parser/parse-card "5d"))))
    (is (= five-of-diamonds (:card (parser/parse-card "d5"))))
    (is (= ten-of-hearts (:card (parser/parse-card "10h"))))
    (is (= ten-of-hearts (:card (parser/parse-card "h10"))))
    (is (= jack-of-clubs (:card (parser/parse-card "jc"))))
    (is (= jack-of-clubs (:card (parser/parse-card "cj"))))))

(deftest parse-valid-card-case-insensitive-test
  (testing "valid cards parsed correctly from different cases"
    (is (= ace-of-spades (:card (parser/parse-card "as"))))
    (is (= ace-of-spades (:card (parser/parse-card "As"))))
    (is (= ace-of-spades (:card (parser/parse-card "aS"))))
    (is (= ace-of-spades (:card (parser/parse-card "AS"))))))

(defn- error-with-message-and-index?
  "Returns true if the result is an error with the given message and index."
  [result message index]
  (and (= message (get-in result [:error :message]))
       (= index (get-in result [:error :index]))))

(deftest parse-invalid-card-test
  (testing "cards invalid return an error"
    (is (error-with-message-and-index? (parser/parse-card "") "Missing values" 0))
    (is (error-with-message-and-index? (parser/parse-card "a") "Missing suit" 1))
    (is (error-with-message-and-index? (parser/parse-card "10") "Missing suit" 2))
    (is (error-with-message-and-index? (parser/parse-card "s") "Missing rank" 1))
    (is (error-with-message-and-index? (parser/parse-card "sz") "Unexpected rank" 1))
    (is (error-with-message-and-index? (parser/parse-card "9z") "Unexpected suit" 1))
    (is (error-with-message-and-index? (parser/parse-card "10z") "Unexpected suit" 2))
    (is (error-with-message-and-index? (parser/parse-card "asdf") "Unexpected value(s)" 2))
    (is (error-with-message-and-index? (parser/parse-card "sadf") "Unexpected value(s)" 2))))

(deftest parse-valid-cards-test
  (testing "valid cards parsed correctly"
    (is (= {:cards [] :errors []} (parser/parse-cards "")))
    (is (= {:cards [ace-of-spades] :errors []} (parser/parse-cards "as")))
    ;; TODO support this as input (no spaces between inputs)
    ;; (is (= {:cards [ace-of-spades jack-of-clubs] :errors []} (parse-cards "asjc")))
    (is (= {:cards [ace-of-spades jack-of-clubs] :errors []} (parser/parse-cards "as jc")))
    (is (= {:cards [ace-of-spades jack-of-clubs] :errors []} (parser/parse-cards "as     jc")))))

;; TODO tests for parse-cards with invalid values
